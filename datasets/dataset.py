from abc import ABC, abstractmethod
from collections import Counter, defaultdict
from pathlib import Path
from typing import Callable, Optional, Sequence, Tuple, Union

import numpy as np
import torch
from torch.nn.functional import one_hot
from torch_geometric.data import Data, Dataset

MOD_ROOM_CLASS = {
    0: "living_room",
    1: "kitchen",
    2: "bedroom",
    3: "bathroom",
    4: "closet",
    5: "balcony",
    6: "corridor",
    # 7: "dining_room",
}

MOD_ROOM_CLASS_COLORS = [
    (124, 84, 38),  # 0: living_room
    (102, 209, 255),  # 1: kitchen"
    (160, 214, 6),  # 2: bedroom"
    (121, 159, 247),  # 3: bathroom"
    (247, 208, 138),  # 4: closet"
    (167, 182, 135),  # 5: balcony"
    (224, 188, 162),  # 6: corridor"
    # (237, 249, 249),  # 7: dining_room"
]


class FloorplanGraphDataset(Dataset, ABC):
    def __init__(self, num_features=6, pre_transform: Optional[Callable] = None):
        super(FloorplanGraphDataset, self).__init__(pre_transform=pre_transform)
        self._subgraphs = []
        self._num_features = num_features

    @abstractmethod
    def load(self, path: Union[str, Path], split: Optional[str] = None) -> None:
        pass

    @abstractmethod
    def class_distribution(self) -> Sequence[Tuple[str, int]]:
        pass

    @abstractmethod
    def original_class_distribution(self) -> Sequence[Tuple[str, int]]:
        pass

    @property
    def subgraphs(self) -> np.ndarray:
        return self._subgraphs

    @property
    def num_node_features(self) -> int:
        return self._num_features

    @property
    def num_classes(self) -> int:
        return len(MOD_ROOM_CLASS)

    @property
    @abstractmethod
    def max_image_size(self) -> np.ndarray:
        pass

    @property
    @abstractmethod
    def test_start_idx(self) -> int:
        pass

    @subgraphs.setter
    def subgraphs(self, val: Sequence[Sequence[np.ndarray]]):
        self._subgraphs = val

    def len(self) -> int:
        return len(self.subgraphs)

    def get_raw(self, index: int) -> Sequence[np.ndarray]:
        return self.subgraphs[index]

    @abstractmethod
    def get_mod_label(self, labels: np.ndarray) -> np.ndarray:
        pass

    @abstractmethod
    def build_graph(self, graph: Sequence[np.ndarray]) -> np.ndarray:
        pass

    @abstractmethod
    def build_features(self, graph: Sequence[np.ndarray]) -> np.ndarray:
        pass

    @abstractmethod
    def get_labels(self, graph: Sequence[np.ndarray]) -> np.ndarray:
        pass

    @abstractmethod
    def get_edges_features(
        self, graph: Sequence[np.ndarray], edges: np.ndarray
    ) -> np.ndarray:
        pass

    def get(self, index: int) -> Data:
        graph = self.subgraphs[index]

        labels = self.get_mod_label(self.get_labels(graph))
        edges = self.build_graph(graph)
        features = self.build_features(graph)

        x = torch.tensor(features, dtype=torch.float)
        edge_index = torch.tensor(edges.T, dtype=torch.long)
        y = torch.tensor(labels, dtype=torch.long)

        y = one_hot(y, self.num_classes).float()

        edges_features = self.get_edges_features(graph, edges)
        edge_attr = torch.tensor(edges_features, dtype=torch.float)

        d = Data(x=x, edge_index=edge_index, y=y, edge_attr=edge_attr)

        if self.pre_transform is not None:
            d = self.pre_transform(d)

        return d

    def intersect(self, A: np.ndarray, B: np.ndarray) -> np.ndarray:
        A, B = A[:, None], B[None]
        low = np.s_[..., :2]
        high = np.s_[..., 2:]
        A, B = A.copy(), B.copy()
        A[high] += 1
        B[high] += 1
        intrs = (
            np.maximum(0, np.minimum(A[high], B[high]) - np.maximum(A[low], B[low]))
        ).prod(-1)
        return intrs  # / ((A[high]-A[low]).prod(-1)+(B[high]-B[low]).prod(-1)-intrs)
