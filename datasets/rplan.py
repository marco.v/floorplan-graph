import math
from collections import defaultdict
from glob import glob
from pathlib import Path
from typing import Callable, Optional, Sequence, Tuple, Union
from xmlrpc.client import Boolean

import cv2
import numpy as np
import rplanpy as rp
from skimage.morphology import skeletonize

from .dataset import MOD_ROOM_CLASS, FloorplanGraphDataset

ACTUAL_ROOM_CLASS = {
    0: "Living room",
    1: "Master room",
    2: "Kitchen",
    3: "Bathroom",
    4: "Dining room",
    5: "Child room",
    6: "Study room",
    7: "Second room",
    8: "Guest room",
    9: "Balcony",
    10: "Entrance",
    11: "Storage",
    12: "Wall-in",
    13: "External area",
    14: "Exterior wall",
    15: "Front door",
    16: "Interior wall",
    17: "Interior door",
}


class RPlanFloorplanGraphDataset(FloorplanGraphDataset):
    def __init__(self, num_features=6, pre_transform: Optional[Callable] = None):
        super(RPlanFloorplanGraphDataset, self,).__init__(
            num_features=num_features, pre_transform=pre_transform
        )
        self._walls_in_room = {}

    def max_image_size(self) -> np.ndarray:
        return np.array([256, 256])

    def test_start_idx(self) -> int:
        return 65000

    def load(self, path: Union[str, Path], split: Optional[str] = None):
        self.path = Path(path)

        if self.path.is_file():
            self.subgraphs = np.load(self.path, allow_pickle=True)
        else:
            self.subgraphs = []
            for img_path in glob(str(self.path / "*.png")):
                plan_data = rp.data.RplanData(img_path)
                # Computing now rooms info and edges acts like a cache
                plan_data.get_rooms()
                plan_data.get_edges()
                plan_data.image = np.array([])
                self.subgraphs.append(plan_data)
            np.save(self.path / "rplan_dataset.npy", self.subgraphs, allow_pickle=True)

        if split == "train":
            self.subgraphs = self.subgraphs[: self.test_start_idx()]
        elif split == "test":
            self.subgraphs = self.subgraphs[self.test_start_idx() :]

        num_nodes = defaultdict(int)
        for g in self.subgraphs:
            labels = g.get_rooms()[:, 4]
            if len(labels) > 0:
                num_nodes[len(labels)] += 1

        print(f"Number of graphs: {len(self.subgraphs)}")
        print(f"Number of graphs by rooms: {num_nodes}")
        print(f"Number of rooms by actual type: {self.original_class_distribution()}")
        print(f"Number of rooms by mod type: {self.class_distribution()}")

    def get_mod_label(self, labels: np.ndarray) -> np.ndarray:
        ROOM_MAPPING = {
            0: 0,  # "Living room",
            1: 2,  # "Master room",
            2: 1,  # "Kitchen",
            3: 3,  # "Bathroom",
            4: 0,  # "Dining room",
            5: 2,  # "Child room",
            6: 0,  # "Study room",
            7: 2,  # "Second room",
            8: 2,  # "Guest room",
            9: 5,  # "Balcony",
            10: 6,  #  "Entrance",
            11: 4,  #  "Storage",
            12: 6,  #  "Wall-in",
            # 13: 0,  #  "External area",
            # 14: 0,  #  "Exterior wall",
            # 15: 0,  #  "Front door",
            # 16: 0,  #  "Interior wall",
            # 17: 0,  #  "Interior door",
        }
        f = lambda l: ROOM_MAPPING[int(l)]
        return np.array([f(label) for label in labels])

    def original_class_distribution(self) -> Sequence[Tuple[str, int]]:
        graph_labels = np.array(
            [l for g in self.subgraphs for l in g.get_rooms()], dtype=np.int32
        )[:, 4]
        class_distribution = [
            (label_name, np.count_nonzero(graph_labels == label))
            for label, label_name in ACTUAL_ROOM_CLASS.items()
        ]
        return class_distribution

    def class_distribution(self) -> Sequence[Tuple[str, int]]:
        graph_labels = np.array(
            [l for g in self.subgraphs for l in g.get_rooms()], dtype=np.int32
        )[:, 4]
        class_distribution = [
            (label_name, np.count_nonzero(self.get_mod_label(graph_labels) == label))
            for label, label_name in MOD_ROOM_CLASS.items()
        ]
        return class_distribution

    def build_graph(self, graph: Sequence[np.ndarray]) -> np.ndarray:
        edges = graph.get_edges()
        return edges[:, :2]

    def get_edges_features(
        self, graph: Sequence[np.ndarray], edges: np.ndarray
    ) -> np.ndarray:
        edge_features = [
            1 if RPlanFloorplanGraphDataset.door_in_edge(graph, edge) else -1
            for edge in edges
        ]
        edge_features = np.array(edge_features)
        return edge_features

    @staticmethod
    def door_in_edge(graph: rp.data.RplanData, edge: list) -> bool:
        """
        Check if there is a door in an edge

        :param edge: edge to check [room1, room2, relation]
        :type edge: list
        :return: door in an edge?
        :rtype: bool
        """
        doors = graph.get_interior_doors()
        room1 = graph.get_rooms()[edge[0]]
        room2 = graph.get_rooms()[edge[1]]
        for door in doors:
            int1 = RPlanFloorplanGraphDataset.rect_intersection(door, room1)
            int2 = RPlanFloorplanGraphDataset.rect_intersection(door, room2)
            if int1 is None or int2 is None:
                continue
            intersection = RPlanFloorplanGraphDataset.rect_intersection(int1, int2)
            if intersection is not None:
                return True
        return False

    @staticmethod
    def rect_intersection(a: np.ndarray, b: np.ndarray) -> Optional[np.ndarray]:
        x1 = max(min(a[0], a[2]), min(b[0], b[2]))
        y1 = max(min(a[1], a[3]), min(b[1], b[3]))
        x2 = min(max(a[0], a[2]), max(b[0], b[2]))
        y2 = min(max(a[1], a[3]), max(b[1], b[3]))
        max_pix_dist = 3
        if (x1 <= x2 or abs(x2 - x1) <= max_pix_dist) and (
            y1 <= y2 or abs(y2 - y1) <= max_pix_dist
        ):
            return np.array([x1, y1, x2, y2])
        return None

    def build_features(self, graph: Sequence[np.ndarray]) -> np.ndarray:
        rooms = graph.get_rooms_with_properties()
        doors = graph.get_interior_doors()
        front_door = graph.get_front_door_mask()
        if doors.size > 0 and front_door.size > 0:
            doors = np.vstack((doors[:, :4], front_door))
        elif doors.size == 0 and front_door.size > 0:
            doors = np.expand_dims(front_door, axis=0)
        features = []

        rooms_bbs = np.array([room["bounding_box"] for room in rooms.values()])
        rooms_bbs_x = rooms_bbs[:, ::2]
        rooms_bbs_y = rooms_bbs[:, 1::2]
        scale_factor_x = rooms_bbs_x.max() - rooms_bbs_x.min()
        scale_factor_y = rooms_bbs_y.max() - rooms_bbs_y.min()

        graph_id = graph.image_path
        if graph_id not in self._walls_in_room:
            self._walls_in_room[graph_id] = [None for _ in rooms]
        for room_idx, bb in enumerate(rooms_bbs):
            rooms_doors = [
                door
                for door in doors
                if RPlanFloorplanGraphDataset.rect_intersection(door, bb) is not None
            ]

            # Get lenght and width normalized according to floorplan size
            width = abs(bb[2] - bb[0]) / scale_factor_x
            lenght = abs(bb[3] - bb[1]) / scale_factor_y
            if lenght < width:
                lenght, width = width, lenght
            if self.num_node_features == 7:
                if self._walls_in_room[graph_id][room_idx] is None:
                    self._walls_in_room[graph_id][room_idx] = len(
                        self._get_room_lines(graph, bb)
                    )
                features.append(
                    [
                        lenght * width,
                        lenght,
                        width,
                        len(rooms_doors),
                        0,
                        0,
                        self._walls_in_room[graph_id][room_idx],
                    ]
                )
            else:
                features.append([lenght * width, lenght, width, len(rooms_doors), 0, 0])
        intersect = self.intersect(rooms_bbs, rooms_bbs)
        for i in range(len(rooms_bbs)):
            for j in range(i + 1, len(rooms_bbs)):
                if intersect[i, j] > 0.7 * intersect[j, j]:
                    if intersect[i, i] > intersect[j, j]:  # is i a parent
                        features[i][5] = 1
                        features[j][4] = 1
                    else:  # i is child
                        features[i][4] = 1
                        features[j][5] = 1
                if intersect[i, j] > 0.7 * intersect[i, i]:
                    if intersect[j, j] > intersect[i, i]:
                        features[j][5] = 1
                        features[i][4] = 1
                    else:
                        features[j][4] = 1
                        features[i][5] = 1
        return np.array(features)

    def get_labels(self, graph: Sequence[np.ndarray]) -> np.ndarray:
        return graph.get_rooms()[:, 4]

    def _get_lines(
        self, graph: Sequence[np.ndarray]
    ) -> Sequence[Tuple[float, float, float, float]]:
        category = graph.category.copy()
        category[category < 14] = 0
        category[category >= 14] = 255
        skeleton = skeletonize(category / 255)
        skeleton = skeleton.astype(np.uint8) * 255

        lines = []
        should_continue = True
        while should_continue:
            should_continue = False
            tmp_lines = cv2.HoughLinesP(
                skeleton,  # Input edge image
                1,  # Distance resolution in pixels
                np.pi / 10,  # Angle resolution in radians
                threshold=3,  # Min number of votes for valid line
                minLineLength=3,  # Min allowed length of line
                maxLineGap=2,  # Max allowed gap between line for joining them
            )
            if tmp_lines is not None and len(tmp_lines) > 0:
                line = None
                for l in tmp_lines:
                    x1, y1, x2, y2 = l[0]
                    cos_angle = math.cos(math.atan2(y2 - y1, x2 - x1))
                    if math.isclose(cos_angle, 0, abs_tol=1e-4) or math.isclose(
                        cos_angle, 1
                    ):
                        line = l[0]
                        break
                if line is None:
                    break
                should_continue = True
                x1, y1, x2, y2 = line
                lines.append(line)
                # remove line
                cv2.line(skeleton, (int(x1), int(y1)), (int(x2), int(y2)), 0, 1)
        return lines

    def segment_intersect_box(
        self,
        box_bl: np.ndarray,
        box_tr: np.ndarray,
        seg_start: np.ndarray,
        seg_end: np.ndarray,
    ) -> Boolean:
        # ref: https://stackoverflow.com/questions/99353/how-to-test-if-a-line-segment-intersects-an-axis-aligned-rectange-in-2d

        # Find min and max X for the segment
        minX = seg_start[0]
        maxX = seg_end[0]

        if seg_start[0] > seg_end[0]:
            minX = seg_end[0]
            maxX = seg_start[0]

        # Find the intersection of the segment's and rectangle's x-projections
        if maxX > box_tr[0]:
            maxX = box_tr[0]

        if minX < box_bl[0]:
            minX = box_bl[0]

        if minX > maxX:  # If their projections do not intersect return false
            return False

        # Find corresponding min and max Y for min and max X we found before
        minY = seg_start[1]
        maxY = seg_end[1]

        dx = seg_end[0] - seg_start[0]

        if abs(dx) > 1e-6:
            a = (seg_end[1] - seg_start[1]) / dx
            b = seg_start[1] - a * seg_start[0]
            minY = a * minX + b
            maxY = a * maxX + b

        if minY > maxY:
            minY, maxY = maxY, minY

        #  Find the intersection of the segment's and rectangle's y-projections
        if maxY > box_tr[1]:
            maxY = box_tr[1]

        if minY < box_bl[1]:
            minY = box_bl[1]

        if minY > maxY:  # If Y-projections do not intersect return false
            return False

        return True

    def _get_room_lines(
        self, graph: Sequence[np.ndarray], room_bb: np.ndarray
    ) -> Sequence[Tuple[float, float, float, float]]:
        lines = self._get_lines(graph)

        # invert xy and dilate bb
        bb = room_bb.copy()
        bb[:2] = bb[:2][::-1] - 3
        bb[2:] = bb[2:][::-1] + 3

        # # Debug
        # aaa = np.zeros((256, 256, 3))
        # cv2.rectangle(aaa, room_bb[:2], room_bb[2:], (0, 0, 255), 1)
        # for line in lines:
        #     cv2.line(aaa, line[:2], line[2:], (255, 0, 0), 1)
        # cv2.imwrite("new_lines.png", aaa)

        room_lines = [
            line
            for line in lines
            if self.segment_intersect_box(bb[:2], bb[2:], line[:2], line[2:])
        ]

        return room_lines
