from collections import defaultdict
from pathlib import Path
from typing import Callable, Optional, Sequence, Tuple, Union
from xmlrpc.client import Boolean

import numpy as np

from .dataset import MOD_ROOM_CLASS, FloorplanGraphDataset

ACTUAL_ROOM_CLASS = {
    1: "living_room",
    2: "kitchen",
    3: "bedroom",
    4: "bathroom",
    5: "missing",
    6: "closet",
    7: "balcony",
    8: "corridor",
    9: "dining_room",
    10: "laundry_room",
}


class HouseGanFloorplanGraphDataset(FloorplanGraphDataset):
    def __init__(
        self, num_features=6, pre_transform: Optional[Callable] = None,
    ):
        super(HouseGanFloorplanGraphDataset, self).__init__(
            num_features=num_features, pre_transform=pre_transform
        )

    def max_image_size(self) -> np.ndarray:
        return np.array([256, 256])

    def test_start_idx(self) -> int:
        return 120000

    def load(self, path: Union[str, Path], split: Optional[str] = None):
        self.path = path

        self.subgraphs = np.load("{}".format(self.path), allow_pickle=True)
        self.subgraphs = self.filter_graphs(self.subgraphs)

        if split == "train":
            self.subgraphs = self.subgraphs[: self.test_start_idx()]
        elif split == "test":
            self.subgraphs = self.subgraphs[self.test_start_idx() :]

        num_nodes = defaultdict(int)
        for g in self.subgraphs:
            labels = g[0]
            if len(labels) > 0:
                num_nodes[len(labels)] += 1

        print(f"Number of graphs: {len(self.subgraphs)}")
        print(f"Number of graphs by rooms: {num_nodes}")
        print(f"Number of rooms by actual type: {self.original_class_distribution()}")
        print(f"Number of rooms by mod type: {self.class_distribution()}")

    def original_class_distribution(self) -> Sequence[Tuple[str, int]]:
        graph_labels = np.array(
            [l for g in self.subgraphs for l in g[0]], dtype=np.int32
        )
        class_distribution = [
            (label_name, np.count_nonzero(graph_labels == label))
            for label, label_name in ACTUAL_ROOM_CLASS.items()
        ]
        return class_distribution

    def class_distribution(self) -> Sequence[Tuple[str, int]]:
        graph_labels = np.array(
            [l for g in self.subgraphs for l in g[0]], dtype=np.int32
        )
        class_distribution = [
            (label_name, np.count_nonzero(self.get_mod_label(graph_labels) == label))
            for label, label_name in MOD_ROOM_CLASS.items()
        ]
        return class_distribution

    def get_mod_label(self, labels: np.ndarray) -> np.ndarray:
        # labels = labels - 1
        # labels[labels >= 5] = labels[labels >= 5] - 1
        # return labels
        ROOM_MAPPING = {
            1: 0,  # "living_room",
            2: 1,  # "kitchen",
            3: 2,  # "bedroom",
            4: 3,  # "bathroom",
            5: -1,  # "missing",
            6: 4,  # "closet",
            7: 5,  # "balcony",
            8: 6,  # "corridor",
            9: 0,  # "dining_room",
            10: -1,  # "laundry_room",
        }
        f = lambda l: ROOM_MAPPING[int(l)]
        return np.array([f(label) for label in labels])

    def get_labels(self, graph: Sequence[np.ndarray]) -> np.ndarray:
        return np.array(graph[0])

    def build_graph(self, graph: Sequence[np.ndarray]) -> np.ndarray:
        rooms_bbs = np.array(graph[1])
        edge2node = [item for sublist in graph[3] for item in sublist]
        node_doors = np.array(edge2node)[graph[4]]
        rooms_bbs_new = []
        for i, bb in enumerate(rooms_bbs):
            x0, y0 = bb[0], bb[1]
            x1, y1 = bb[2], bb[3]
            xmin, ymin = min(x0, x1), min(y0, y1)
            xmax, ymax = max(x0, x1), max(y0, y1)
            rooms_bbs_new.append(np.array([xmin, ymin, xmax, ymax]))
        rooms_bbs = np.stack(rooms_bbs_new)

        rooms_bbs = rooms_bbs / 256.0

        tl = np.min(rooms_bbs[:, :2], 0)
        br = np.max(rooms_bbs[:, 2:], 0)
        shift = (tl + br) / 2.0 - 0.5
        rooms_bbs[:, :2] -= shift
        rooms_bbs[:, 2:] -= shift
        tl -= shift
        br -= shift

        edges = self.get_edges(rooms_bbs)

        return edges

    def get_edges_features(
        self, graph: Sequence[np.ndarray], edges: np.ndarray
    ) -> np.ndarray:
        openings2Walls = graph[4]
        wallToRoomsMapping = graph[3]
        edge_features = np.full(edges.shape[0], -1, dtype=np.float)
        for wall in openings2Walls:
            rooms = wallToRoomsMapping[wall]
            for idx, (room1, room2) in enumerate(edges):
                if room1 in rooms and room2 in rooms:
                    edge_features[idx] = 1.0
        edge_features = np.array(edge_features)
        return edge_features

    def build_features(self, graph: Sequence[np.ndarray]) -> np.ndarray:
        rooms_bbs = np.array(graph[1])
        wallToRoomsMapping = graph[3]
        openings2Walls = graph[4]

        rooms_bbs_x = rooms_bbs[:, ::2]
        rooms_bbs_y = rooms_bbs[:, 1::2]
        scale_factor_x = rooms_bbs_x.max() - rooms_bbs_x.min()
        scale_factor_y = rooms_bbs_y.max() - rooms_bbs_y.min()

        features = []
        for i, bb in enumerate(rooms_bbs):
            x0, y0 = bb[0], bb[1]
            x1, y1 = bb[2], bb[3]
            xmin, ymin = min(x0, x1), min(y0, y1)
            xmax, ymax = max(x0, x1), max(y0, y1)
            lenght = (xmax - xmin) / scale_factor_x
            width = (ymax - ymin) / scale_factor_y
            area = lenght * width
            if lenght < width:
                lenght, width = width, lenght
            walls_in_room = [
                wall for wall, rooms in enumerate(wallToRoomsMapping) if i in rooms
            ]
            doors_count = [1 for wall in openings2Walls if wall in walls_in_room]
            if self.num_node_features == 7:
                features.append(
                    [area, lenght, width, len(doors_count), 0, 0, len(walls_in_room)]
                )
            else:
                features.append([area, lenght, width, len(doors_count), 0, 0])
        intersect = self.intersect(rooms_bbs, rooms_bbs)
        for i in range(len(rooms_bbs)):
            for j in range(i + 1, len(rooms_bbs)):
                if intersect[i, j] > 0.7 * intersect[j, j]:
                    if intersect[i, i] > intersect[j, j]:  # is i a parent
                        features[i][5] = 1
                        features[j][4] = 1
                    else:  # i is child
                        features[i][4] = 1
                        features[j][5] = 1
                if intersect[i, j] > 0.7 * intersect[i, i]:
                    if intersect[j, j] > intersect[i, i]:
                        features[j][5] = 1
                        features[i][4] = 1
                    else:
                        features[j][4] = 1
                        features[i][5] = 1
        return np.array(features)

    def get_edges(self, bbs: np.ndarray) -> np.ndarray:
        edges = []
        for k in range(len(bbs)):
            for l in range(len(bbs)):
                if l > k:
                    bb0 = bbs[k]
                    bb1 = bbs[l]
                    if self.is_adjacent(bb0, bb1):
                        edges.append([k, l])
                        edges.append([l, k])
        edges = np.array(edges)
        return edges

    def filter_graphs(
        self, graphs: Sequence[Sequence[np.ndarray]]
    ) -> Sequence[Sequence[np.ndarray]]:
        new_graphs = []
        for g in graphs:
            labels = g[0]
            rooms_bbs = g[1]
            # discard broken samples
            check_none = np.sum([bb is None for bb in rooms_bbs])
            check_node = np.sum([nd == 0 for nd in labels])
            edges = self.build_graph(g)
            if len(labels) < 2 or len(edges) < 1 or check_none > 0 or check_node > 0:
                continue
            new_graphs.append(g)
        return new_graphs

    def is_adjacent(
        self, box_a: np.ndarray, box_b: np.ndarray, threshold: float = 0.03
    ) -> Boolean:

        x0, y0, x1, y1 = box_a
        x2, y2, x3, y3 = box_b

        h1, h2 = x1 - x0, x3 - x2
        w1, w2 = y1 - y0, y3 - y2

        xc1, xc2 = (x0 + x1) / 2.0, (x2 + x3) / 2.0
        yc1, yc2 = (y0 + y1) / 2.0, (y2 + y3) / 2.0

        delta_x = np.abs(xc2 - xc1) - (h1 + h2) / 2.0
        delta_y = np.abs(yc2 - yc1) - (w1 + w2) / 2.0

        delta = max(delta_x, delta_y)

        return delta < threshold
