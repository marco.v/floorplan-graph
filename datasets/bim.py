from typing import Callable, Optional, Sequence

import numpy as np

from .housegan import HouseGanFloorplanGraphDataset


class DreamcatcherFloorplanGraphDataset(HouseGanFloorplanGraphDataset):
    def __init__(
        self, num_features=6, pre_transform: Optional[Callable] = None,
    ):
        super(DreamcatcherFloorplanGraphDataset, self).__init__(
            num_features=num_features, pre_transform=pre_transform
        )

    def test_start_idx(self) -> int:
        return 0

    def get_mod_label(self, labels: np.ndarray) -> np.ndarray:
        return labels

    def filter_graphs(
        self, graphs: Sequence[Sequence[np.ndarray]]
    ) -> Sequence[Sequence[np.ndarray]]:
        new_graphs = []
        for g in graphs:
            labels = g[0]
            rooms_bbs = g[1]
            # discard broken samples
            check_none = np.sum([bb is None for bb in rooms_bbs])
            edges = self.build_graph(g)
            if len(labels) < 2 or len(edges) < 1 or check_none > 0:
                continue
            new_graphs.append(g)
        return new_graphs
