## Room Classification on Floor Plans

This project is inspired and partially based on the following paper [paper](https://arxiv.org/abs/2108.05947).


Datasets: 

- [Housegan](https://www.dropbox.com/sh/p707nojabzf0nhi/AAB4UPwW0EgHhbQuHyq60tCKa?dl=0&preview=housegan_clean_data.npy)  

- [RPlan](http://staff.ustc.edu.cn/~fuxm/projects/DeepLayout/index.html)

