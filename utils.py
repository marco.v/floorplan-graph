import json
from pathlib import Path
from typing import Dict, Sequence, Union

import matplotlib.patches as patches
import matplotlib.pyplot as plt
import networkx as nx
import numpy as np
import torch
from datasets.bim import DreamcatcherFloorplanGraphDataset
from datasets.dataset import MOD_ROOM_CLASS, FloorplanGraphDataset
from models.crawl.data_utils import preproc
from models.model import Model
from torch_geometric.loader import DataLoader
from torch_geometric.utils import to_networkx
from tqdm import tqdm

device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")


def visualize(d, bbs=None, colors=None, saveGraph=False, savePath=""):
    G = to_networkx(d, to_undirected=True)
    plt.figure(figsize=(7, 7))
    plt.axis("off")
    labels = {i: MOD_ROOM_CLASS[int(d.y[i])] for i in range(len(d.y))}
    if colors is None:
        colors = plt.get_cmap("Dark2").colors
    else:
        colors = np.array(colors)
        if colors.max() > 1:
            colors = colors / 255
    colors = [colors[i] for i in d.y]
    nx.draw_networkx(
        G,
        pos=nx.spring_layout(G, seed=42),
        with_labels=True,
        labels=labels,
        node_color=colors,
        cmap="Dark2",
    )
    if saveGraph:
        plt.savefig(savePath)
    else:
        plt.show()
    if bbs is not None:
        plt.figure(figsize=(7, 7))
        for i, (xmin, ymin, xmax, ymax) in enumerate(bbs):
            rect = patches.Rectangle(
                (xmin, ymin),
                xmax - xmin,
                ymax - ymin,
                edgecolor="k",
                facecolor=c[d.y[i]],
                alpha=0.9,
            )
            plt.gca().add_patch(rect)
        if saveGraph:
            plt.savefig(savePath)
        else:
            plt.show()


def metrics(
    model: Model, dataloader: DataLoader, nb_classes: int
) -> Dict[str, Union[float, np.ndarray]]:
    confusion_matrix = torch.zeros(nb_classes, nb_classes, dtype=torch.long)

    model.to(device)
    model.eval()
    for data in tqdm(dataloader):
        data = data.to(device)

        out = model(data)

        pred = out.argmax(1)
        y_true = data.y.argmax(1)

        idx = torch.stack((y_true.view(-1), pred.view(-1)), dim=1)
        idx, idx_count = idx.unique(dim=0, return_counts=True)
        confusion_matrix[idx[:, 0], idx[:, 1]] += idx_count.cpu()

    tp = confusion_matrix.diag()
    fp = confusion_matrix.sum(dim=0) - tp
    fn = confusion_matrix.sum(dim=1) - tp

    precision = tp / (tp + fp)
    recall = tp / (tp + fn)

    f1_score = 2 * (precision * recall) / (precision + recall)

    overall_accuracy = tp.sum() / confusion_matrix.sum()

    return {
        "accuracy": overall_accuracy.item(),
        "class_recall": recall.cpu().detach().numpy(),
        "class_precision": precision.cpu().detach().numpy(),
        "class_f1_score": f1_score.cpu().detach().numpy(),
    }


def get_graph_from_BIM(
    path: Union[str, Path],
    floorplan_max_image_width: int = 256,
    floorplan_max_image_height: int = 256,
) -> Sequence[np.ndarray]:
    with open(path) as f:
        json_data = json.load(f)
    rooms_labels = json_data["roomsLabels"]
    roomsBBs = json_data["roomsBBs"]
    wallEdges = json_data["wallEdges"]
    wallToRoomsMapping = json_data["wallToRoomsMapping"]
    openingsToWalls = json_data["openingsToWalls"]

    rooms_labels = [label if label in MOD_ROOM_CLASS else 0 for label in rooms_labels]
    roomsBBs = [
        [roomBB["x0"], roomBB["y0"], roomBB["x1"], roomBB["y1"]] for roomBB in roomsBBs
    ]
    wallEdges = [
        [wallEdge["x0"], wallEdge["y0"], wallEdge["x1"], wallEdge["y1"], 0, 0]
        for wallEdge in wallEdges
    ]
    wallToRoomsMapping = [
        wallToRoomMap["mapping"] for wallToRoomMap in wallToRoomsMapping
    ]

    roomsBBs = np.array(roomsBBs)
    wallEdges = np.array(wallEdges)
    edges_x = wallEdges[:, 0:-2:2]
    edges_y = wallEdges[:, 1:-2:2]
    conversion_factor = max(
        (np.max(edges_x) - np.min(edges_x)) * 1.1 / floorplan_max_image_width,
        (np.max(edges_y) - np.min(edges_y)) * 1.1 / floorplan_max_image_height,
    )

    wallEdges = wallEdges.astype(np.float64)
    roomsBBs = roomsBBs.astype(np.float64)
    wallEdges = wallEdges / conversion_factor
    roomsBBs = roomsBBs / conversion_factor

    edges_x = wallEdges[:, 0:-2:2]
    edges_y = wallEdges[:, 1:-2:2]
    shift = [
        (np.max(edges_x) + np.min(edges_x)) / 2.0 - floorplan_max_image_width / 2,
        (np.max(edges_y) + np.min(edges_y)) / 2.0 - floorplan_max_image_height / 2,
    ]
    shift = np.tile(shift, (1, 2))
    wallEdges[:, :4] -= shift
    roomsBBs -= shift

    ds_entry = [
        rooms_labels,
        roomsBBs,
        wallEdges,
        wallToRoomsMapping,
        openingsToWalls,
    ]

    return ds_entry


def load_BIM_dataset(path: Union[str, Path], num_features=6) -> FloorplanGraphDataset:

    dataset = DreamcatcherFloorplanGraphDataset(
        num_features=num_features, pre_transform=preproc
    )
    floorplan_max_image_width, floorplan_max_image_height = dataset.max_image_size()

    path = Path(path)
    if path.is_dir():
        files = [f for f in path.glob("*.json")]
    elif path.is_file():
        files = [path]
    else:
        raise FileNotFoundError()

    subgraphs = [
        get_graph_from_BIM(f, floorplan_max_image_width, floorplan_max_image_height)
        for f in files
    ]

    dataset.subgraphs = subgraphs

    return dataset
