input_file=$1
output_path=$2
dataset_type=$3

if [ -z "$input_file" ]; then 
    input_file=/mnt/c/BIM/Dreamcatcher/Saved/Logs/DsEntry.txt
fi

if [ -z "$output_path" ]; then 
    output_path=/mnt/c/users/marco.venturelli/Pictures/test
fi

if [ -z "$dataset_type" ]; then 
    dataset_type="bim"
fi

echo "Input File: $input_file"
echo "Output path File: $output_path"
echo "Dataset: $dataset_type"

cp $input_file $output_path/

python run.py --dataset_file $input_file --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/wrong_class_weights/results_crawl_7/CRaWl_epoch31.pth --num_features 7 --outpath $output_path/crawl_7 &

python run.py --dataset_file $input_file --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/wrong_class_weights/results_crawl_6/CRaWl_epoch42.pth --num_features 6 --outpath $output_path/crawl_6 &

python run.py --dataset_file $input_file --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/results_crawl_7_new/CRaWl_epoch63.pth --num_features 7 --outpath $output_path/crawl_7_new &

wait

python run.py --dataset_file $input_file --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/results_crawl_6_new/CRaWl_epoch37.pth --num_features 6 --outpath $output_path/crawl_6_new &

python run.py --dataset_file $input_file --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/wrong_class_weights/results_mixed_7/Model_epoch43.pth --num_features 7 --outpath $output_path/sage_7_old_weights --config config2.json &

python run.py --dataset_file $input_file --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_7/Model_epoch26.pth --num_features 7 --outpath $output_path/sage_7_vanilla --config config2.json &

wait

python run.py --dataset_file $input_file --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_7_deep_4/Model_epoch6.pth --num_features 7 --outpath $output_path/sage_7 --config config.json &

python run.py --dataset_file $input_file --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_7_deep_6/Model_epoch29.pth --num_features 7 --outpath $output_path/sage_7_2 --config config1.json &

python run.py --dataset_file $input_file --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_6_deep_4/Model_epoch28.pth --num_features 6 --outpath $output_path/sage_6 --config config.json &

wait

python run.py --dataset_file $input_file --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/no_weights/results_crawl_7/CRaWl_epoch34.pth --num_features 7 --outpath $output_path/crawl_7_noweights &

python run.py --dataset_file $input_file --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/no_weights/results_crawl_6/CRaWl_epoch35.pth --num_features 6 --outpath $output_path/crawl_6_new_noweights &

python run.py --dataset_file $input_file --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/no_weights/results_sage_7/Model_epoch35.pth --num_features 7 --outpath $output_path/sage_7_noweights --config config.json &

wait

python run.py --dataset_file $input_file --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/no_weights/results_sage_7_6layer/Model_epoch31.pth --num_features 7 --outpath $output_path/sage_7_2_noweights --config config1.json &

python run.py --dataset_file $input_file --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/no_weights/results_sage_6/Model_epoch35.pth --num_features 6 --outpath $output_path/sage_6_noweights --config config.json &

python run.py --dataset_file $input_file --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_7_6layer_drop/Model_epoch28.pth --num_features 7 --outpath $output_path/sage_7_2_noweights_drop --config config1_drop.json &

wait

# python run.py --dataset_file $input_file --dataset_type $dataset_type --model dwgnn --checkpoint /home/marco/Projects/floorplan-graph/results_dwgnn/DWGNN_epoch18.pth --num_features 6 --outpath $output_path/dwgnn --config config1.json &

# python run.py --dataset_file $input_file --dataset_type $dataset_type --model graphsage --checkpoint /home/marco/Projects/floorplan-graph/results_graphsage/GraphSAGE_epoch15.pth --num_features 7 --outpath $output_path/graphsage --config config1.json &

python run.py --dataset_file $input_file --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_7_6layer_saint/Model_epoch20.pth --num_features 7 --outpath $output_path/sage_7_2_noweights_saint --config config1.json &

wait