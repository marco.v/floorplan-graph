import argparse
import json
import pathlib

import numpy as np
import torch
from datasets.dataset import MOD_ROOM_CLASS
from datasets.housegan import HouseGanFloorplanGraphDataset
from datasets.rplan import RPlanFloorplanGraphDataset
from models.crawl.data_utils import CRaWlLoader, preproc
from models.crawl.model_crawl import CRaWl
from models.model import Model
from torch.nn import CrossEntropyLoss, Linear
from torch_geometric.loader import DataLoader
from torch_geometric.nn import GATv2Conv, GCNConv, SAGEConv, TAGConv
from tqdm import tqdm
from utils import metrics

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--model",
        choices=["mlp", "gcn", "gat", "sage", "tagcn", "crawl"],
        default="sage",
        help="Type of model",
    )
    parser.add_argument(
        "--epoch", type=int, default=100, help="Number of epochs to train"
    )
    parser.add_argument(
        "--config",
        type=str,
        default="config.json",
        help="Path to JSON file with network configuration",
    )
    parser.add_argument("--lr", type=float, default=0.001, help="Learning rate")
    parser.add_argument(
        "--step",
        type=int,
        default=5,
        help="Step size or patience for learning rate scheduling",
    )
    parser.add_argument(
        "--gamma",
        type=float,
        default=0.5,
        help="Decay rate for learning rate scheduling",
    )
    parser.add_argument("--bs", type=int, default=128, help="Batch size for training")
    parser.add_argument(
        "--outpath", type=str, default="./results", help="Path to save results"
    )
    parser.add_argument(
        "--dataset_path",
        type=str,
        nargs="+",
        help="Path to dataset. For House-GAN it must be the path to the .npy file path. For RPlan it can be either the .npy file or the directory containing all the images. If you want to use both datasets, you need to pass both paths. The first path must be the path to House-GAN dataset, then the path to RPlan dataset.",
    )
    parser.add_argument(
        "--dataset_type",
        choices=["housegan", "rplan", "mixed"],
        help="Type of dataset.",
    )
    parser.add_argument(
        "--checkpoint", type=str, default=None, help="Path to model checkpoint"
    )
    parser.add_argument(
        "--checkpoint_freq",
        type=int,
        default=5,
        help="How often to save a model checkpoint",
    )
    parser.add_argument(
        "--num_features",
        type=int,
        choices=[6, 7],
        default=6,
        help="Number of node features",
    )
    parser.add_argument(
        "--w_loss",
        default=False,
        action="store_true",
        help="Use a weighted loss according to the number of graph nodes with a certain class.",
    )
    args = parser.parse_args()
    models = {
        "mlp": Linear,
        "gcn": GCNConv,
        "gat": GATv2Conv,  # GATConv,
        "sage": SAGEConv,
        "tagcn": TAGConv,
        "crawl": CRaWl,
    }
    datasets = {
        "housegan": HouseGanFloorplanGraphDataset,
        "rplan": RPlanFloorplanGraphDataset,
    }
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    print(device)

    outpath = pathlib.Path(args.outpath)
    outpath.mkdir(parents=True, exist_ok=True)

    torch.manual_seed(42)

    with open(args.config, "r") as f:
        config = json.load(f)

    num_classes = len(MOD_ROOM_CLASS)
    if args.model == "crawl":
        model = CRaWl(
            config,
            node_feat_dim=args.num_features,
            edge_feat_dim=1,
            out_dim=num_classes,
        )
        data_loader = CRaWlLoader
    else:
        n_hidden = config["hidden"]
        hidden_features = config["hidden_features"]
        classifier = config["graph_out"]
        edge_dropout = config["edge_dropout"] if "edge_dropout" in config else 0.0
        model = Model(
            layer_type=models[args.model],
            n_hidden=n_hidden,
            in_features=args.num_features,
            out_classes=num_classes,
            hidden_features=hidden_features,
            classifier=classifier,
            drop_edges=edge_dropout,
        )
        data_loader = DataLoader
    print(model)

    pytorch_total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    print(f"Number of parameters: {pytorch_total_params}")

    if args.checkpoint is not None and pathlib.Path(args.checkpoint).is_file():
        print(f"loading checkpoint {args.checkpoint}")
        model.load_state_dict(torch.load(args.checkpoint))

    model = model.to(device)

    if args.dataset_type != "mixed":
        assert len(args.dataset_path) == 1
        dataset_path = args.dataset_path[0]
        train_dataset = datasets[args.dataset_type](
            num_features=args.num_features, pre_transform=preproc
        )
        train_dataset.load(path=dataset_path, split="train")

        test_dataset = datasets[args.dataset_type](
            num_features=args.num_features, pre_transform=preproc
        )
        test_dataset.load(path=dataset_path, split="test")

        if args.w_loss:
            class_distribution = train_dataset.class_distribution()
            class_distribution = torch.tensor(
                [w for l, w in class_distribution], dtype=torch.float
            )
            class_weights = class_distribution.sum() / (
                class_distribution * num_classes
            )
            print(f"class weights: {class_weights}")
        else:
            class_weights = None
    else:
        assert len(args.dataset_path) == 2
        housegan_path = args.dataset_path[0]
        rplan_path = args.dataset_path[1]

        dataset_house = HouseGanFloorplanGraphDataset(
            num_features=args.num_features, pre_transform=preproc
        )
        dataset_rplan = RPlanFloorplanGraphDataset(
            num_features=args.num_features, pre_transform=preproc
        )
        dataset_house.load(housegan_path, split="train")
        dataset_rplan.load(rplan_path, split="train")
        train_dataset = dataset_house + dataset_rplan

        if args.w_loss:
            class_distribution1 = dataset_house.class_distribution()
            class_distribution1 = torch.tensor(
                [w for l, w in class_distribution1], dtype=torch.float
            )

            class_distribution2 = dataset_rplan.class_distribution()
            class_distribution2 = torch.tensor(
                [w for l, w in class_distribution2], dtype=torch.float
            )

            class_distribution = class_distribution1 + class_distribution2
            class_weights = class_distribution.sum() / (
                class_distribution * num_classes
            )
            print(f"class weights: {class_weights}")
        else:
            class_weights = None

        test_dataset_house = HouseGanFloorplanGraphDataset(
            num_features=args.num_features, pre_transform=preproc
        )
        test_dataset_rplan = RPlanFloorplanGraphDataset(
            num_features=args.num_features, pre_transform=preproc
        )
        test_dataset_house.load(housegan_path, split="test")
        test_dataset_rplan.load(rplan_path, split="test")
        test_dataset = test_dataset_house + test_dataset_rplan

    trainloader = data_loader(
        train_dataset, batch_size=args.bs, shuffle=True, num_workers=4
    )
    testloader = data_loader(test_dataset, batch_size=args.bs, num_workers=4)

    num_epochs = args.epoch
    lr = args.lr
    step_size = args.step
    gamma = args.gamma

    if class_weights is not None:
        class_weights = class_weights.to(device)

    criterion = CrossEntropyLoss(weight=class_weights)
    optimizer = torch.optim.Adam(model.parameters(), lr=lr)
    lr_scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimizer, factor=gamma, patience=step_size, verbose=True
    )

    loss_ep = []
    te_acc_ep = []
    tr_acc_ep = []
    best_val_accuracy = 0.0
    for epoch in range(num_epochs):
        model.train()
        loss = 0
        correct = 0
        num_nodes = 0
        for data in tqdm(trainloader):
            data = data.to(device)
            optimizer.zero_grad()

            out = model(data)

            loss_ = criterion(out, data.y)

            loss_.backward()
            optimizer.step()
            loss += loss_.item()

            # train accuracy
            pred = out.argmax(1)
            y_true = data.y.argmax(1)
            correct += sum(pred == y_true)
            num_nodes += data.num_nodes
        loss /= len(trainloader)
        tr_acc = (correct / num_nodes).item()
        test_metrics = metrics(model, testloader, num_classes)
        te_acc = test_metrics["accuracy"]
        lr_scheduler.step(1 - te_acc)

        loss_ep.append(loss)
        tr_acc_ep.append(tr_acc)
        te_acc_ep.append(
            [
                te_acc,
                *test_metrics["class_recall"].tolist(),
                *test_metrics["class_precision"].tolist(),
                *test_metrics["class_f1_score"].tolist(),
            ]
        )
        print(
            f'Epoch [{epoch+1}/{num_epochs}] Loss: {loss:.6f}, Train Acc: {tr_acc:.4}, Test Acc: {te_acc:.4f}, Test F1: {test_metrics["class_f1_score"].mean():.4f}, Test Class Acc: {np.array2string(test_metrics["class_recall"], precision=4)}'
        )

        if te_acc > best_val_accuracy:
            best_val_accuracy = te_acc
            torch.save(
                model.state_dict(),
                outpath / f"{type(model).__name__}_epoch{epoch}.pth",
            )
        elif epoch % args.checkpoint_freq == 0:
            torch.save(
                model.state_dict(),
                outpath / f"{type(model).__name__}_epoch{epoch}_.pth",
            )

    result = np.concatenate(
        (
            np.expand_dims(loss_ep, axis=-1),
            np.expand_dims(tr_acc_ep, axis=-1),
            np.array(te_acc_ep),
        ),
        axis=-1,
    )
    np.savetxt(
        outpath
        / f"{type(model).__name__}_loss_tracc_teacc_{lr}_{num_epochs}_{step_size}_{gamma}.txt",
        result,
        fmt="%.6e",
    )
    max_idx = result[:, 2].argmax()
    print(f"\nMax Test Accuracy at Epoch {max_idx+1}: {result[max_idx]}\n")

    torch.save(
        model.state_dict(), outpath / f"{type(model).__name__}_epoch{num_epochs}.pth",
    )
