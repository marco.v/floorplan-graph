
# python train.py --dataset_path data/housegan_clean_data.npy data/rplan_dataset.npy --dataset_type mixed --epoch 50 --bs 64 --model crawl --outpath no_weights/results_crawl_7 --num_features 7 

# python train.py --dataset_path data/housegan_clean_data.npy data/rplan_dataset.npy --dataset_type mixed --epoch 35 --bs 64 --model crawl --outpath no_weights/results_crawl_6 --num_features 6

# python train.py --dataset_path data/housegan_clean_data.npy data/rplan_dataset.npy --dataset_type mixed --epoch 35 --bs 64 --model sage --outpath no_weights/results_sage_7 --num_features 7

# python train.py --dataset_path data/housegan_clean_data.npy data/rplan_dataset.npy --dataset_type mixed --epoch 35 --bs 64 --model sage --outpath no_weights/results_sage_6 --num_features 6

# python train.py --dataset_path data/housegan_clean_data.npy data/rplan_dataset.npy --dataset_type mixed --epoch 35 --bs 64 --model sage --outpath no_weights/results_sage_7_6layer --num_features 7 --config config1.json


# python train.py --dataset_path data/housegan_clean_data.npy data/rplan_dataset.npy --dataset_type mixed --epoch 30 --bs 64 --model sage --outpath results_sage_7_6layer_drop --num_features 7 --config config1_drop.json

# python train.py --dataset_path data/housegan_clean_data.npy data/rplan_dataset.npy --dataset_type mixed --epoch 30 --bs 64 --model graphsage --outpath results_graphsage --num_features 7 --config config1.json

# python train.py --dataset_path data/housegan_clean_data.npy data/rplan_dataset.npy --dataset_type mixed --epoch 30 --bs 64 --model dwgnn --outpath results_dwgnn --num_features 6 --config config1.json --checkpoint results_dwgnn/DWGNN_epoch2.pth

python train.py --dataset_path data/housegan_clean_data.npy data/rplan_dataset.npy --dataset_type mixed --epoch 30 --bs 64 --model sage --outpath results_sage_7_6layer_saint --num_features 7 --config config1.json