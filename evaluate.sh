
# dataset_path="data/housegan_clean_data.npy data/rplan_dataset.npy"
# dataset_type="mixed"
dataset_path="data/DC/bim_dataset.npy"
dataset_type="bim"

echo "#####################################################"
echo "crawl_7"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/wrong_class_weights/results_crawl_7/CRaWl_epoch31.pth --num_features 7
echo "#####################################################"
echo "crawl_6"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/wrong_class_weights/results_crawl_6/CRaWl_epoch42.pth  --num_features 6
echo "#####################################################"
echo "crawl_7_new"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/results_crawl_7_new/CRaWl_epoch63.pth --num_features 7 
echo "#####################################################"
echo "crawl_6_new"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/results_crawl_6_new/CRaWl_epoch37.pth --num_features 6 
echo "#####################################################"
echo "sage_7_old_weigths"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/wrong_class_weights/results_mixed_7/Model_epoch43.pth --num_features 7 --config config2.json
echo "#####################################################"
echo "sage_7_vanilla"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_7/Model_epoch26.pth --num_features 7 --config config2.json
echo "#####################################################"
echo "sage_7"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_7_deep_4/Model_epoch6.pth --num_features 7  --config config.json
echo "#####################################################"
echo "sage_7_2"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_7_deep_6/Model_epoch29.pth --num_features 7 --config config1.json
echo "#####################################################"
echo "sage_6"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_6_deep_4/Model_epoch28.pth --num_features 6 --config config.json
echo "#####################################################"
echo "No WEIGHTS crawl_7_new"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/no_weights/results_crawl_7/CRaWl_epoch34.pth --num_features 7 
echo "#####################################################"
echo "No WEIGHTS crawl_6_new"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model crawl --checkpoint /home/marco/Projects/floorplan-graph/no_weights/results_crawl_6/CRaWl_epoch35.pth --num_features 6 
echo "#####################################################"
echo "No WEIGHTS sage_7"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/no_weights/results_sage_7/Model_epoch35.pth --num_features 7  --config config.json
echo "#####################################################"
echo "No WEIGHTS sage_6"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/no_weights/results_sage_6/Model_epoch35.pth --num_features 6 --config config.json
echo "#####################################################"
echo "No WEIGHTS sage_7_2"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/no_weights/results_sage_7_6layer/Model_epoch31.pth --num_features 7 --config config1.json
echo "#####################################################"
echo "No WEIGHTS sage_7_2 Edge Dropout"
echo "#####################################################"
python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_7_6layer_drop/Model_epoch28.pth --num_features 7 --config config1_drop.json
# echo "#####################################################"
# echo "No WEIGHTS DWGNN"
# echo "#####################################################"
# python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model dwgnn --checkpoint /home/marco/Projects/floorplan-graph/results_dwgnn/DWGNN_epoch18.pth --num_features 6 --config config1.json
# echo "#####################################################"
# echo "No WEIGHTS graphsage_7"
# echo "#####################################################"
# python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model graphsage --checkpoint /home/marco/Projects/floorplan-graph/results_graphsage/GraphSAGE_epoch15.pth --num_features 7 --config config1.json
# echo "#####################################################"
# echo "No WEIGHTS sage_7_2 GraphSAINT"
# echo "#####################################################"
# python evaluate.py --dataset_path $dataset_path --dataset_type $dataset_type --model sage --checkpoint /home/marco/Projects/floorplan-graph/results_sage_7_6layer_saint/Model_epoch20.pth --num_features 7 --config config1.json
