from modulefinder import Module
from typing import Optional

import torch
import torch.nn.functional as F
from torch.nn import Linear, Sequential
from torch_geometric.data import Data
from torch_geometric.utils import dropout_adj


class Model(torch.nn.Module):
    def __init__(
        self,
        layer_type: str,
        n_hidden: int = 2,
        out_classes: int = 8,
        in_features: int = 6,
        hidden_features: int = 32,
        classifier: str = "linear",
        drop_edges: float = 0.0,
    ):
        super(Model, self).__init__()
        torch.manual_seed(42)
        self.layer_type = layer_type
        self.layer1 = self.init_layer(in_features, hidden_features, edge_dim=1)
        self.layer2 = torch.nn.ModuleList()
        self.drop_edges = drop_edges
        for _ in range(n_hidden - 1):
            self.layer2.append(
                self.init_layer(hidden_features, hidden_features, edge_dim=1)
            )

        if classifier == "linear":
            self.classifier = Linear(hidden_features, out_classes)
        else:
            self.classifier = Sequential(
                Linear(hidden_features, hidden_features),
                Linear(hidden_features, out_classes),
            )

    def init_layer(
        self, in_channels: int, out_channels: int, edge_dim: Optional[int] = None
    ):
        layer_name = self.layer_type.__name__

        simple_init = lambda in_channels, out_channels, edge_dim: self.layer_type(
            in_channels, out_channels
        )
        edge_dim_init = lambda in_channels, out_channels, edge_dim: self.layer_type(
            in_channels, out_channels, edge_dim=edge_dim
        )

        layer_init_f = {
            "Linear": simple_init,
            "GCNConv": simple_init,
            "GATConv": edge_dim_init,
            "GATv2Conv": edge_dim_init,
            "SAGEConv": simple_init,
            "TAGConv": simple_init,
        }
        return layer_init_f[layer_name](in_channels, out_channels, edge_dim)

    def layer_forward(
        self,
        layer: Module,
        x: torch.tensor,
        edge_index: torch.tensor,
        edge_attr: Optional[torch.tensor] = None,
    ) -> torch.tensor:
        layer_name = self.layer_type.__name__

        f_x = lambda x, edge_index, edge_attr: layer(x)
        f_x_e = lambda x, edge_index, edge_attr: layer(x, edge_index)
        f_x_e_a = lambda x, edge_index, edge_attr: layer(x, edge_index, edge_attr)

        layer_f = {
            "Linear": f_x,
            "GCNConv": f_x_e_a,
            "GATConv": f_x_e_a,
            "GATv2Conv": f_x_e_a,
            "SAGEConv": f_x_e,
            "TAGConv": f_x_e_a,
        }
        return layer_f[layer_name](x, edge_index, edge_attr)

    def forward(self, data: Data) -> torch.tensor:

        x, edge_index, edge_attr = data.x, data.edge_index, data.edge_attr

        edge_index, edge_attr = dropout_adj(
            edge_index, edge_attr, p=self.drop_edges, training=self.training
        )

        h = self.layer_forward(self.layer1, x, edge_index, edge_attr)
        h = F.relu(h)
        for layer in self.layer2:
            h = self.layer_forward(layer, h, edge_index, edge_attr)
            h = F.relu(h)
        out = self.classifier(h)
        return out
