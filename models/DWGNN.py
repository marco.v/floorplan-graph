# https://github.com/LymanSong/FP_GNN

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from dgl import from_networkx
from dgl.utils import expand_as_pair
from sklearn import preprocessing
from torch_geometric.data import Data
from torch_geometric.utils import to_networkx


def scaling(x):
    scaler = preprocessing.MinMaxScaler()
    if len(x.shape) != 2:
        x = x.reshape(-1, 1)
    return scaler.fit_transform(x)


class PyTMinMaxScaler(object):
    """
    Transforms each channel to the range [0, 1].
    """

    def __call__(self, tensor):
        tensor = tensor.clone()
        if tensor.shape[-1] == 1:
            tensor = torch.squeeze(tensor, dim=-1)
        for ch in tensor:
            if len(ch) != 1:
                min_ = ch.min(dim=0)[0]
                scale = ch.max(dim=0)[0].sub(min_)
                ch.sub_(min_).div_(scale)
        return torch.unsqueeze(tensor, dim=-1)


scaler = PyTMinMaxScaler()


class EWConv(nn.Module):
    def __init__(
        self, in_feats, out_feats, edge_func, aggregator_type="mean", device="cpu"
    ):
        super().__init__()
        self._in_src_feats, self._in_dst_feats = expand_as_pair(in_feats)
        self.device = device
        self.out_feats = out_feats
        self.edge_func = edge_func
        self.aggregator_type = aggregator_type
        self.pool_func = nn.Linear(self._in_src_feats, self.out_feats)
        if aggregator_type == "lstm":
            self.lstm = nn.LSTM(self.out_feats, self.out_feats, batch_first=True)
        self.self_func = nn.Linear(self._in_src_feats, self.out_feats)
        self.reset_parameters()

    def reset_parameters(self):
        gain = nn.init.calculate_gain("relu")
        nn.init.xavier_uniform_(self.pool_func.weight, gain=gain)
        if self.aggregator_type == "lstm":
            self.lstm.reset_parameters()
        # nn.init.xavier_uniform_(self.fc_neigh.weight, gain=gain)

    def udf_edge(self, edges):
        return {"edge_features": edges.data["w"], "neighbors": edges._src_data["h"]}

    def udf_u_mul_e(self, nodes):
        m = self.edge_func
        weights = nodes.mailbox["edge_features"]

        # weights = torch.div(weights.squeeze(dim = 2), weights.sum(1)).unsqueeze(dim = 2)
        # soft_ed = m(weights)
        # soft_ed = m(scaler(weights))
        # soft_ed_ = m(scaler(weights))
        soft_ed = m(
            torch.FloatTensor(
                np.squeeze(
                    np.apply_along_axis(scaling, 1, weights.cpu().numpy()), axis=2
                )
            )
        ).to(self.device)

        res = soft_ed.unsqueeze(-1) * nodes.mailbox["neighbors"]
        if self.aggregator_type == "sum":
            res = res.sum(axis=1)
        elif self.aggregator_type == "mean":
            res = res.mean(axis=1)
        elif self.aggregator_type == "max":
            res = res.max(axis=1)[0]
        elif self.aggregator_type == "lstm":
            batch_size = res.shape[0]
            hid = (
                res.new_zeros((1, batch_size, self.out_feats)),
                res.new_zeros((1, batch_size, self.out_feats)),
            )
            _, (res, _) = self.lstm(res, hid)  # only get hidden state
            res = res.permute(1, 0, 2)
        return {"h_reduced": res}

    def forward(self, graph, feat, efeat):
        with graph.local_scope():
            feat_src, feat_dst = expand_as_pair(feat, graph)
            graph.srcdata["h"] = self.pool_func(feat_src)
            graph.edata["w"] = efeat
            graph.update_all(self.udf_edge, self.udf_u_mul_e)
            result = self.self_func(feat_dst) + graph.dstdata["h_reduced"].squeeze()

            return result


class DWGNN(nn.Module):
    def __init__(
        self,
        in_feats,
        hid_feats,
        out_feats,
        edge_feats,
        num_layers,
        aggregator_type,
        device,
    ):
        super().__init__()
        self.num_layers = num_layers
        self.layers = torch.nn.ModuleList()
        self.batch_norms = torch.nn.ModuleList()
        # self.edge_funcs = torch.nn.ModuleList()
        if aggregator_type not in ["sum", "mean", "max", "lstm"]:
            raise KeyError("invalid aggregator type(sum, mean, max)")
        for layer in range(self.num_layers - 1):
            if layer == 0:
                # self.edge_funcs.append(torch.nn.Linear(edge_feats, in_feats * hid_feats))
                self.layers.append(
                    EWConv(
                        in_feats=in_feats,
                        out_feats=hid_feats,
                        edge_func=nn.Softmin(dim=1),
                        aggregator_type=aggregator_type,
                        device=device,
                    )
                )
            else:
                # self.edge_funcs.append(torch.nn.Linear(edge_feats, hid_feats*hid_feats))
                self.layers.append(
                    EWConv(
                        in_feats=hid_feats,
                        out_feats=hid_feats,
                        edge_func=nn.Softmin(dim=1),
                        aggregator_type=aggregator_type,
                        device=device,
                    )
                )
        # self.edge_funcs.append(torch.nn.Linear(edge_feats, hid_feats * out_feats))
        self.layers.append(
            EWConv(
                in_feats=hid_feats,
                out_feats=out_feats,
                edge_func=nn.Softmin(dim=1),
                aggregator_type=aggregator_type,
                device=device,
            )
        )

        for layer in range(self.num_layers - 1):
            self.batch_norms.append(nn.BatchNorm1d((hid_feats)))

    # def forward(self, graph, inputs, edge_features, batch_norm=True):
    def forward(self, data: Data, batch_norm=True):

        x, edge_index, edge_attr = data.x, data.edge_index, data.edge_attr
        g = to_networkx(data)
        graph = from_networkx(g, device=x.device)

        h = x
        e = edge_attr
        for i in range(self.num_layers):
            if i != self.num_layers - 1:
                h = F.relu(self.layers[i](graph, h, e))
                if batch_norm == True:
                    h = self.batch_norms[i](h)
            else:
                h = self.layers[i](graph, h, e)

        h = F.log_softmax(h, dim=1)

        return h
