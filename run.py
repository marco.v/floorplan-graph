import argparse
import json
import os
import pathlib
import random
from ctypes import ArgumentError

import cv2
import numpy as np
import torch
from datasets.bim import DreamcatcherFloorplanGraphDataset
from datasets.dataset import (
    MOD_ROOM_CLASS,
    MOD_ROOM_CLASS_COLORS,
    FloorplanGraphDataset,
)
from datasets.housegan import HouseGanFloorplanGraphDataset
from datasets.rplan import RPlanFloorplanGraphDataset
from models.crawl.data_utils import merge_batch, preproc
from models.crawl.model_crawl import CRaWl
from models.model import Model
from torch.nn import Linear
from torch_geometric.nn import GATv2Conv, GCNConv, SAGEConv, TAGConv
from utils import load_BIM_dataset, visualize

random.seed(123)


def predict(model: torch.nn.Module, data: torch.tensor) -> np.ndarray:
    data = data.to(device)

    out = model(data)

    print(out)

    out = out.cpu().detach().numpy()

    out_labels = np.argmax(out, axis=-1)
    print(out_labels)
    return out_labels


def get_poly(edges: np.ndarray) -> np.ndarray:
    lines = np.reshape(edges, (-1, 4))
    lines = np.around(lines).astype(np.int32)
    lines = lines.tolist()

    poly = []
    first_point = np.array(lines.pop(0))
    poly.append(first_point[:2])
    poly.append(first_point[2:])

    while len(lines) > 0:
        last_point = poly[-1]
        found_next_point = False
        for l in lines:
            x0 = np.array(l[:2])
            x1 = np.array(l[2:])
            if cv2.norm(last_point, x0) == 0:
                poly.append(x1)
                lines.remove(l)
                found_next_point = True
                break
            elif cv2.norm(last_point, x1) == 0:
                poly.append(x0)
                lines.remove(l)
                found_next_point = True
                break
        if not found_next_point:
            # raise ValueError("Next polygon point not found")
            break

    return poly[:-1]


def get_image(
    dataset: FloorplanGraphDataset, index: int, predictions: np.ndarray = None
) -> np.ndarray:
    if isinstance(dataset, HouseGanFloorplanGraphDataset):
        return get_image_housegan(dataset, index, predictions)
    elif isinstance(dataset, RPlanFloorplanGraphDataset):
        return get_image_rplan(dataset, index, predictions)
    else:
        raise ArgumentError("Dataset not valid")


def get_image_housegan(
    dataset: HouseGanFloorplanGraphDataset, index: int, predictions: np.ndarray = None
) -> np.ndarray:
    graph = dataset.get_raw(index)

    labels = np.array(graph[0])
    rooms_bbs = graph[1]
    wall_edges = np.array(graph[2])
    edges2rooms = graph[3]
    opening2edge = graph[4]

    img = np.full((256, 256, 3), 255, dtype=np.uint8)
    img_bb = np.full((256, 256, 3), 255, dtype=np.uint8)

    radius = 1
    point_thickness = 2
    line_thickness = 1
    point_color = (255, 0, 0)
    line_color = (255, 128, 0)
    opening_color = (0, 0, 255)

    labels = dataset.get_mod_label(labels)

    if predictions is not None:
        labels = predictions

    rooms_color = [(color[2], color[1], color[0]) for color in MOD_ROOM_CLASS_COLORS]

    for room_idx, room_bb in enumerate(rooms_bbs):
        x0 = (round(room_bb[0]), round(room_bb[1]))
        x1 = (round(room_bb[2]), round(room_bb[3]))

        pred_label = int(labels[room_idx])

        cv2.rectangle(img_bb, x0, x1, rooms_color[pred_label], -1)

        edges_idx = [
            edge_idx
            for edge_idx, edge2room in enumerate(edges2rooms)
            if room_idx in edge2room
        ]
        edges = wall_edges[edges_idx, :4]
        poly = get_poly(edges)
        cv2.drawContours(
            img, [np.array(poly)], 0, rooms_color[pred_label], thickness=-1
        )

    for edge_idx, edge in enumerate(wall_edges):
        x0 = (round(edge[0]), round(edge[1]))
        x1 = (round(edge[2]), round(edge[3]))

        cv2.circle(img, x0, radius, point_color, point_thickness)
        cv2.circle(img, x1, radius, point_color, point_thickness)
        cv2.line(img, x0, x1, line_color, line_thickness)

        if edge_idx in opening2edge:
            cv2.circle(
                img,
                ((x0[0] + x1[0]) // 2, (x0[1] + x1[1]) // 2),
                radius,
                opening_color,
                point_thickness,
            )

    return img, img_bb


def get_image_rplan(
    dataset: RPlanFloorplanGraphDataset, index: int, predictions: np.ndarray = None
) -> np.ndarray:
    graph = dataset.get_raw(index)

    def get_color(label):
        if label == 13:
            return (255, 255, 255)
        elif label in [14, 16]:
            return (255, 128, 0)
        elif label in [15, 17]:
            return (0, 0, 255)
        else:
            color = MOD_ROOM_CLASS_COLORS[int(dataset.get_mod_label([label]))]
            return (color[2], color[1], color[0])

    labels = dataset.get_labels(graph)
    labels = dataset.get_mod_label(labels)
    if predictions is not None:
        labels = predictions

    rooms_color = [(color[2], color[1], color[0]) for color in MOD_ROOM_CLASS_COLORS]

    img_bb = np.full((256, 256, 3), 255, dtype=np.uint8)
    for room_idx, room_bb in enumerate(graph.get_rooms()):
        x0 = (round(room_bb[1]), round(room_bb[0]))
        x1 = (round(room_bb[3]), round(room_bb[2]))
        pred_label = int(labels[room_idx])
        cv2.rectangle(img_bb, x0, x1, rooms_color[pred_label], -1)

    floorplan_img = [[get_color(pix) for pix in row] for row in graph.category]
    floorplan_img = np.array(floorplan_img, dtype=np.uint8)
    floorplan_img.astype(np.uint8)

    if predictions is not None:
        segment_img = graph.category.copy()
        segment_img[segment_img >= 14] = 255
        segment_img[segment_img < 255] = 0
        segment_img = cv2.cvtColor(segment_img, cv2.COLOR_GRAY2BGR)

        for room_idx, room_bb in enumerate(graph.get_rooms()):
            seed_img = np.ones(segment_img.shape[:2], dtype=np.int32)
            x0 = np.array([round(room_bb[1]), round(room_bb[0])]) - 1
            x1 = np.array([round(room_bb[3]), round(room_bb[2])]) + 1
            # unknown region to 0
            cv2.rectangle(seed_img, x0, x1, 0, -1)
            room_center = (x0 + x1) // 2
            cv2.circle(seed_img, room_center, 2, 255, -1)
            markers = cv2.watershed(segment_img, seed_img)

            room_img = np.zeros(segment_img.shape[:2], dtype=np.uint8)
            room_img[markers == 255] = 255
            contours, _ = cv2.findContours(
                room_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
            )

            pred_label = int(labels[room_idx])
            cv2.drawContours(
                floorplan_img, contours, 0, rooms_color[pred_label], cv2.FILLED
            )

    return floorplan_img, img_bb


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--model",
        choices=["mlp", "gcn", "gat", "sage", "tagcn", "crawl"],
        default="sage",
        help="Type of model",
    )
    parser.add_argument(
        "--config",
        type=str,
        default="config.json",
        help="Path to JSON file with network configuration",
    )
    parser.add_argument("--checkpoint", type=str, help="Path to model checkpoint")
    parser.add_argument(
        "--outpath", type=str, default="./results", help="Path to save results"
    )
    parser.add_argument(
        "--dataset_file",
        type=str,
        default="./data/housegan_clean_data.npy",
        help="House-GAN dataset .npy file path",
    )
    parser.add_argument(
        "--index",
        type=int,
        default=0,
        help="Index of the sample inside the dataset you want to use as input",
    )
    parser.add_argument(
        "--dataset_type",
        choices=["housegan", "rplan", "bim"],
        default="housegan",
        help="Type of dataset.",
    )
    parser.add_argument(
        "--num_features",
        type=int,
        choices=[6, 7],
        default=6,
        help="Number of node features",
    )
    args = parser.parse_args()
    models = {
        "mlp": Linear,
        "gcn": GCNConv,
        "gat": GATv2Conv,  # GATConv,
        "sage": SAGEConv,
        "tagcn": TAGConv,
        "crawl": CRaWl,
    }
    datasets = {
        "housegan": HouseGanFloorplanGraphDataset,
        "rplan": RPlanFloorplanGraphDataset,
        "bim": DreamcatcherFloorplanGraphDataset,
    }
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    print(device)

    out_path = pathlib.Path(args.outpath)
    out_path.mkdir(parents=True, exist_ok=True)

    torch.manual_seed(42)

    with open(args.config, "r") as f:
        config = json.load(f)

    num_classes = len(MOD_ROOM_CLASS)
    if args.model == "crawl":
        model = CRaWl(
            config,
            node_feat_dim=args.num_features,
            edge_feat_dim=1,
            out_dim=num_classes,
        )
    else:
        n_hidden = config["hidden"]
        hidden_features = config["hidden_features"]
        classifier = config["graph_out"]
        model = Model(
            layer_type=models[args.model],
            n_hidden=n_hidden,
            in_features=args.num_features,
            out_classes=num_classes,
            hidden_features=hidden_features,
            classifier=classifier,
        )
    print(model)

    model.load_state_dict(torch.load(args.checkpoint))

    model = model.to(device)
    model.eval()

    dataset_file = pathlib.Path(args.dataset_file)
    if dataset_file.suffix == ".csv" or dataset_file.suffix == ".txt":
        dataset = load_BIM_dataset(dataset_file, num_features=args.num_features)
        index = 0
    else:
        dataset = datasets[args.dataset_type](
            num_features=args.num_features, pre_transform=preproc
        )
        dataset.load(path=args.dataset_file, split="test")
        index = args.index

    data = dataset[index]
    data = merge_batch([data])
    out = predict(model, data)

    img, img_bb = get_image(dataset, index, out)
    cv2.imwrite(str(out_path / "pred.png"), img)
    cv2.imwrite(str(out_path / "bbs.png"), img_bb)

    data.y = out
    visualize(
        data,
        colors=MOD_ROOM_CLASS_COLORS,
        saveGraph=True,
        savePath=out_path / "graph.png",
    )

    if len(dataset) > 1:
        # Collect some example from dataset
        out_base_path = out_path / "ds"
        if not os.path.exists(out_base_path):
            os.mkdir(out_base_path)
        for index in range(min(100, len(dataset))):
            data = dataset[index]
            out_sample_path = out_base_path / str(index)
            if not os.path.exists(out_sample_path):
                os.mkdir(out_sample_path)

            img, img_bb = get_image(dataset, index)
            cv2.imwrite(str(out_sample_path / "gt.png"), img)
            cv2.imwrite(str(out_sample_path / "gt_bbs.png"), img_bb)

            graph_path = out_sample_path / "gt_graph.png"
            gt_data = data
            gt_data.y = gt_data.y.argmax(1)
            visualize(
                data, colors=MOD_ROOM_CLASS_COLORS, saveGraph=True, savePath=graph_path,
            )

            # save predictions
            data = merge_batch([data])
            out = predict(model, data)

            img, img_bb = get_image(dataset, index, out)
            cv2.imwrite(str(out_sample_path / "pred.png"), img)
            cv2.imwrite(str(out_sample_path / "bbs.png"), img_bb)

            graph_path = out_sample_path / "graph.png"
            data.y = out
            visualize(
                data, colors=MOD_ROOM_CLASS_COLORS, saveGraph=True, savePath=graph_path,
            )
