import argparse
import json
from ctypes import ArgumentError
from typing import Union

import torch
from datasets.bim import DreamcatcherFloorplanGraphDataset
from datasets.dataset import MOD_ROOM_CLASS
from datasets.housegan import HouseGanFloorplanGraphDataset
from datasets.rplan import RPlanFloorplanGraphDataset
from models.crawl.data_utils import CRaWlLoader, preproc
from models.crawl.model_crawl import CRaWl
from models.model import Model
from torch.nn import Linear
from torch_geometric.loader import DataLoader
from torch_geometric.nn import GATv2Conv, GCNConv, SAGEConv, TAGConv
from utils import metrics

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "--model",
        choices=["mlp", "gcn", "gat", "sage", "tagcn", "crawl"],
        default="sage",
        help="Type of model",
    )
    parser.add_argument(
        "--config",
        type=str,
        default="config.json",
        help="Path to JSON file with network configuration",
    )
    parser.add_argument("--checkpoint", type=str, help="Path to model checkpoint")
    parser.add_argument(
        "--dataset_path",
        type=str,
        nargs="+",
        help="Path to dataset. For House-GAN it must be the path to the .npy file path. For RPlan it can be either the .npy file or the directory containing all the images. If you want to use both datasets, you need to pass both paths. The first path must be the path to House-GAN dataset, then the path to RPlan dataset.",
    )
    parser.add_argument(
        "--dataset_type",
        choices=["housegan", "rplan", "mixed", "bim"],
        default="housegan",
        help="Type of dataset.",
    )
    parser.add_argument(
        "--num_features",
        type=int,
        choices=[6, 7],
        default=6,
        help="Number of node features",
    )
    args = parser.parse_args()
    models = {
        "mlp": Linear,
        "gcn": GCNConv,
        "gat": GATv2Conv,  # GATConv,
        "sage": SAGEConv,
        "tagcn": TAGConv,
        "crawl": CRaWl,
    }
    datasets = {
        "housegan": HouseGanFloorplanGraphDataset,
        "rplan": RPlanFloorplanGraphDataset,
        "bim": DreamcatcherFloorplanGraphDataset,
    }
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
    print(device)

    torch.manual_seed(42)

    with open(args.config, "r") as f:
        config = json.load(f)

    num_classes = len(MOD_ROOM_CLASS)
    if args.model == "crawl":
        model = CRaWl(
            config,
            node_feat_dim=args.num_features,
            edge_feat_dim=1,
            out_dim=num_classes,
        )
        data_loader = CRaWlLoader
    else:
        n_hidden = config["hidden"]
        hidden_features = config["hidden_features"]
        classifier = config["graph_out"]
        model = Model(
            layer_type=models[args.model],
            n_hidden=n_hidden,
            in_features=args.num_features,
            out_classes=num_classes,
            hidden_features=hidden_features,
            classifier=classifier,
        )
        data_loader = DataLoader
    print(model)

    model.load_state_dict(torch.load(args.checkpoint))

    model = model.to(device)
    model.eval()

    if args.dataset_type != "mixed":
        assert len(args.dataset_path) == 1
        dataset_path = args.dataset_path[0]

        test_dataset = datasets[args.dataset_type](
            num_features=args.num_features, pre_transform=preproc
        )
        test_dataset.load(path=dataset_path, split="test")
    else:
        assert len(args.dataset_path) == 2
        housegan_path = args.dataset_path[0]
        rplan_path = args.dataset_path[1]

        dataset_house = HouseGanFloorplanGraphDataset(
            num_features=args.num_features, pre_transform=preproc
        )
        dataset_rplan = RPlanFloorplanGraphDataset(
            num_features=args.num_features, pre_transform=preproc
        )

        test_dataset_house = HouseGanFloorplanGraphDataset(
            num_features=args.num_features, pre_transform=preproc
        )
        test_dataset_rplan = RPlanFloorplanGraphDataset(
            num_features=args.num_features, pre_transform=preproc
        )
        test_dataset_house.load(housegan_path, split="test")
        test_dataset_rplan.load(rplan_path, split="test")
        test_dataset = test_dataset_house + test_dataset_rplan

    testloader = data_loader(test_dataset, batch_size=64, num_workers=4)

    test_metrics = metrics(model, testloader, num_classes)

    test_acc = test_metrics["accuracy"]
    recall = test_metrics["class_recall"]
    precision = test_metrics["class_precision"]
    f1_score = test_metrics["class_f1_score"]

    print(f"Accuracy: {test_acc:.5f}")
    print(f"F1-Score: {f1_score.mean():.5f}")
    print(f"Avg Recall: {recall.mean():.5f}")
    print(f"Avg Precision: {precision.mean():.5f}")
    print("Class metrics")
    print("\t" + " " * 12 + "\trec\tprec\tf1_score")
    for i, label in enumerate(MOD_ROOM_CLASS.values()):
        print(f"\t{label:12}\t{recall[i]:.5f}\t{precision[i]:.5f}\t{f1_score[i]:.5f}")
