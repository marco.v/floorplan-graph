import os
import shutil
from pathlib import Path
from typing import Union

import numpy as np
from utils import load_BIM_dataset


def copy_ds_entry_files(in_path: Union[str, Path], out_path: Union[str, Path]):

    if not out_path.exists():
        os.mkdir(out_path)

    for subdir in os.listdir(in_path):
        ds_entry = in_path / subdir / "DsEntry.txt"
        if not os.path.isfile(ds_entry):
            continue

        print(ds_entry)
        shutil.copyfile(ds_entry, out_path / f"{subdir}.json")


def create_DS(in_path: Union[str, Path]):

    dataset = load_BIM_dataset(in_path)

    np.save(in_path / "bim_dataset.npy", dataset.subgraphs, allow_pickle=True)


if __name__ == "__main__":

    in_path = Path("/mnt/c/Users/marco.venturelli/Pictures/out_tests/")
    new_ds_path = Path("data/DC")

    # copy_ds_entry_files(in_path, new_ds_path)

    create_DS(new_ds_path)
